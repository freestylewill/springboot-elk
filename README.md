# springboot-elk

#### 介绍
基于ELK6.3.0+SpringBoot+Logback实现日志查看demo

### logback.xml的配置

```
<?xml version="1.0" encoding="UTF-8"?>
<configuration>
    <appender name="LOGSTASH" class="net.logstash.logback.appender.LogstashTcpSocketAppender">
        <destination>127.0.0.1:4560</destination>
        <encoder charset="UTF-8" class="net.logstash.logback.encoder.LogstashEncoder" >
            <customFields>{"appname":"springboot-elk-demo"}</customFields>
        </encoder>
    </appender>
    <appender name="STDOUT" class="ch.qos.logback.core.ConsoleAppender">
        <encoder charset="UTF-8">
            <pattern>%d{HH:mm:ss.SSS} [%thread] %-5level %logger - %msg%n</pattern>
        </encoder>
    </appender>

    <root level="INFO">
        <appender-ref ref="LOGSTASH" />
        <appender-ref ref="STDOUT" />
    </root>
</configuration>

```


### elasticsearch配置

下载 elasticsearch-6.3.0.tar.gz

`wget https://artifacts.elastic.co/downloads/elasticsearch/elasticsearch-6.3.0.tar.gz`

压缩 

`tar -zxvf elasticsearch-6.3.0.tar.gz`

修改config

`vim ./config/elasticsearch.yml`

 添加基本配置

```
    cluster.name: my-app
    node.name: node-1
    #本机ip
    network.host: 192.168.1.110
    #默认监听端口
    http.port: 9200
```

启动

`nohup ./bin/elasticsearch &`

可以访问一下，效果如下：

`http://192.168.1.110:9200`

![elasticsearch启动成功](https://images.gitee.com/uploads/images/2019/1229/200521_60799662_764599.png "701d77e5-3a21-4480-ae37-f32b5505cb3a.png")

### logstash配置

下载安装包

`wget https://artifacts.elastic.co/downloads/logstash/logstash-6.3.0.tar.gz`

解压

`tar -zxvf logstash-6.3.0.tar.gz `

修改config：

`vim config/logstash.conf `


```
input {
 tcp {
  host => "0.0.0.0"
  port => 4560
  mode => "server"
  codec => "json"
 }
}
output {
  elasticsearch {
  hosts => ["192.168.1.110:9200"]
  index => "%{[appname]}-%{+YYYY.MM.dd}"
 }
 stdout {
  codec => rubydebug { }
 }
}
```


注意：

1.input.tcp: 中配置的是本机地址，ip和端口必须和springboot的logback.xml中的配置完全一样，不能一个配ip一个localhost

2.output.elasticsearch: 配置elasticsearch服务器的ip

3.%{appname}: 引用springboot的logback.xml中配置的appname变量

4.output.stdout: 在终端显示输出信息（可以不配置）

启动logstash 

`nohup ./bin/logstash -f ./config/logstash.conf &`

### kibana配置

下载安装包：

```
wget https://artifacts.elastic.co/downloads/kibana/kibana-6.3.0-linux-x86_64.tar.gz

```

解压

`tar -zxvf kibana-6.3.0-linux-x86_64.tar.gz`

修改config： 

```
vim config/kibana.yml
server.host: "0.0.0.0"
elasticsearch.url: "http://192.168.1.110:9200"
elasticsearch.username: "elastic"
elasticsearch.password: "changeme"
```

启动

`nohup ./bin/kibana &`

访问本机5601端口，比如我的机子就访问192.168.1.110:5601就能看到kibana

![输入图片说明](https://images.gitee.com/uploads/images/2019/1229/200717_e5987091_764599.png "kibana1.png")

### 配置kibana，查看日志

我们打开192.168.1.110:5601后，能看见基本界面啦。但是还需要来配置一番，接下来就直接上图，因为是比较简单的操作
![输入图片说明](https://images.gitee.com/uploads/images/2019/1229/200917_12492f97_764599.png "906c5de6-93ba-452a-85af-a96cbaf97da3.png")
![输入图片说明](https://images.gitee.com/uploads/images/2019/1229/200936_b3ec29ce_764599.png "44f72cb7-d666-4203-b33f-74799f025595.png")
![输入图片说明](https://images.gitee.com/uploads/images/2019/1229/200946_dd904873_764599.png "7fb39935-246a-4634-9f88-240a682a306c.png")
![输入图片说明](https://images.gitee.com/uploads/images/2019/1229/201010_ec4adc09_764599.png "a7d3abfc-a916-4366-863e-f7a151e3e395.png")
